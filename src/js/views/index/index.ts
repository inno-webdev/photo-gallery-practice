import './index.scss';

document.getElementById('openMenu')
    .addEventListener('click', function () {
    document.body.classList.add('menu-open');
});

document.getElementById('closeMenu')
    .addEventListener('click', function () {
    document.body.classList.remove('menu-open');
});
